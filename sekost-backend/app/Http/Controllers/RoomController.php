<?php

namespace App\Http\Controllers;

use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $rooms = Room::all();
        return view('dashboard.rooms.index', compact('rooms'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('dashboard.rooms.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request){
    $validatedData = $request->validate([
        'location' => 'required',
        'price' => 'required|numeric',
        'facilities' => 'required|array',
        'facilities.*' => 'distinct',
        'photos' => 'required|array',
        'photos.*' => 'image|mimes:jpeg,png,jpg'
    ]);

    $room = new Room();
    $room->location = $request->input('location');
    $room->price = $request->input('price');
    $facilities = $request->input('facilities');
    $facilitiesString = implode(',', $facilities);
    $room->facilities = $facilitiesString;

    $uploadedPhotos = [];
    if ($request->hasFile('photos')) {
        foreach ($request->file('photos') as $photo) {
            $photo_ekstensi = $photo->extension();
            $photo_name = date('ymdhis') . "-" . uniqid() . "." . $photo_ekstensi;
            $photo->move(public_path('photo'), $photo_name);
            $uploadedPhotos[] = $photo_name;
        }
    }
    $room->photo = implode(',', $uploadedPhotos);

    $room->save();

    return redirect()->route('rooms.index')->with('success', 'Kamar Berhasil Ditambah');
}


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $room = Room::findOrfail($id);
        return view('dashboard.rooms.show', compact('room'));
            // $room = Room::findOrFail($id);
            // return view('rooms.show', compact('room'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        // $room = Room::findOrFail($id);
        // return view('rooms.edit', compact('room'));
        $room = Room::findOrFail($id);
        return view('dashboard.rooms.edit', compact('room'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $room = Room::findOrFail($id);


        $request->validate([
            'location' => 'required',
            'price' => 'required|numeric',
            'facilities' => 'required|array',
            'photo' => 'required|image|mimes:png,jpg,jpeg'
        ]);


        // hapus poto sebelumnya jika ada dan upload foto baru
        if($request->hasFile('photo')){
            // hapus foto sebelumnya
            if($room->photo && file_exists(public_path('photo/' . $room->photo))){
                unlink(public_path('photo/' . $room->photo));
            }
            $photo = $request->file('photo');
            $photo_ekstensi = $photo->extension();
            $photo_name = date('ymdhis'). "." . $photo_ekstensi;
            $photo->move(public_path('photo'), $photo_name);

            $room->photo = $photo_name;
        }


        $room->location = $request->input('location');
        $room->price = $request->input('price');
        $room->facilities = implode(',',$request->input('facilities'));

        $room->save();

        return redirect()->route('rooms.index')->with('success', 'Data Kamar Berhasil Ditambah');
      
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
