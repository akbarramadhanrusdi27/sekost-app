<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use HasFactory;

    protected $fillable = ['price', 'facilities', 'location', 'photo'];


    public function getRoomDetail()
    {
        // Logika untuk mendapatkan detail kamar
        $detail = [
            'location' => $this->location,
            'price' => $this->price,
            'facilities' => $this->facilities,
            'photos' => $this->photo,
            // Tambahkan detail lainnya sesuai kebutuhan
        ];

        return $detail;
    }
}
