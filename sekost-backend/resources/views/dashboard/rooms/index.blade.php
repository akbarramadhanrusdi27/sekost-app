<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css"
        integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

    <title>Data Kamar Kost</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-dark">
        <div class="container">

            <a class="navbar-brand text-white" href="#">Dashboard SEKOST</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav m-auto">
                    <a class="nav-link text-white ml-2" href="#">Kamar Kost </a>
                    <a class="nav-link text-white ml-2" href="#">Features</a>
                    <a class="nav-link text-white ml-2" href="#">Pricing</a>

                </div>
                <a class="nav-link disabled">
                    <form action="{{ route('logout') }}" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-primary">Logout</button>
                    </form>
                </a>
    </nav>
    </div>

    </div>


    <div class="container mt-5">
        <a href="{{ route('rooms.create') }}" class="btn btn-primary mb-5">Tambah Kamar</a>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">NO</th>
                    <th scope="col">Lokasi</th>
                    <th scope="col">Harga</th>
                    <th scope="col">Fasilitas</th>
                    <th scope="col">Foto</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($rooms as $room )

                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $room->location }}</td>
                        <td>{{ 'Rp ' . number_format($room->price, 0, ',', '.') }}
                        </td>
                        <td>
                            @php
                                $facilities = explode(',', $room->facilities);
                            @endphp
                            @foreach($facilities as $facility)
                                <span>{{ $facility }}</span><br>
                            @endforeach
                        </td>

                        <td>
                            <div id="carousel-{{ $loop->iteration }}" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    @php
                                        $photos = explode(',', $room->photo);
                                    @endphp
                                    @foreach($photos as $key => $photo)
                                        <div
                                            class="carousel-item {{ $key === 0 ? 'active' : '' }}">
                                            <img src="{{ asset('photo/' . $photo) }}"
                                                alt="Foto Kamar" height="100">
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </td>
                        <td>
                            <a href="{{ route('rooms.show', $room->id) }}"
                                class="btn btn-primary">Detail</a>
                            <a href="{{ route('rooms.edit', $room->id) }}"
                                class="btn btn-success">Edit</a>
                            <form action="{{ route('rooms.destroy', $room->id) }}"
                                method="POST" style="display: inline-block">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Hapus</button>
                            </form>
                        </td>

                    </tr>

                @endforeach

            </tbody>
        </table>
    </div>




    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous">
    </script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>
    -->
</body>

</html>
